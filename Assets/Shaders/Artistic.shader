﻿Shader "Art/Artistic"
{
	Properties
	{
		_OutlineFloat4("Outline Colour", Color) = (0,0,0,1)
		_OutlineWidth("Outline Width", Range(0.0, 2.0)) = 0.05
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_MainColor("Tint", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		//LOD 100  // NO LOD

		// Outline Pass
		Pass
		{
			// Cull
			Cull Front
			ZWrite On

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			uniform float _OutlineWidth;
			uniform float4 _OutlineFloat4;

			struct v2f 
			{
				float4 pos : POSITION;
				float4 col : COLOR;
			};

			v2f vert(appdata_base vIn)
			{
				v2f vOut;
				vOut.pos = mul(UNITY_MATRIX_MVP, vIn.vertex);

				float3 normal = mul((float3x3) UNITY_MATRIX_IT_MV, vIn.normal);
				float2 offset = TransformViewToProjection(normal.xy);

				//vOut.pos.xy += offset * _OutlineWidth;
				vOut.col = _OutlineFloat4;
				return vOut;
			}

			fixed4 frag (v2f fIn) : SV_Target
			{
				return fIn.col;
			}
			ENDCG

		}

		// Standard Pass
		/*Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				//UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			uniform float4 _MainColor;
			
			v2f vert (appdata_full v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				//o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.color = v.color;
				//UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = _MainColor;
				//fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}*/


		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

			sampler2D _MainTex;

			struct Input {
				float2 uv_MainTex;
			};

			half _Glossiness;
			half _Metallic;
			fixed4 _MainColor;

			void surf(Input IN, inout SurfaceOutputStandard o) {
				// Albedo comes from a texture tinted by color
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _MainColor;
				//fixed4 c = _MainColor;
				o.Albedo = c.rgb;
				// Metallic and smoothness come from slider variables
				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness;
				o.Alpha = c.a;
			}
			ENDCG


	}
	FallBack "Diffuse"
}
