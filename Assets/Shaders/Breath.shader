﻿Shader "Ball/Breath"
{
	Properties
	{
		_ColorLow("Low Colour", Color) = (0,0,0,0)
		_ColorHigh("High Colour", Color) = (1,1,1,1)
		_OutlineWidth("Outline Width", Range(0.0, 2.0)) = 0.05
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_MainColor("Tint", Color) = (1,1,1,1)
	}
		SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		//LOD 100  // NO LOD
		Blend SrcAlpha OneMinusSrcAlpha
		// Outline Pass
		Pass
	{
		Cull Off
		//ZWrite Off
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag		


#include "UnityCG.cginc"
#pragma target 3.0
		uniform float _OutlineWidth;
	uniform float4 _ColorHigh;
	uniform float4 _ColorLow;

	struct v2f
	{
		float4 pos : POSITION;
		float4 col : COLOR;
	};

	v2f vert(appdata_base vIn)
	{
		v2f vOut;
		vOut.pos = mul(UNITY_MATRIX_MVP, vIn.vertex);

		float3 normal = mul((float3x3) UNITY_MATRIX_IT_MV, vIn.normal);
		float2 offset = TransformViewToProjection(normal.xy);

		vOut.pos.xy += offset * (_SinTime.w + 1.0) * 0.25;
		vOut.col = vIn.texcoord;
		//vOut.col.xyz = Vector3.Lerp(_ColorLow.xyz,_ColorHigh.xyz,(_SinTime.w + 1.0)/2);
		return vOut;
	}

	fixed4 frag(v2f fIn) : SV_Target
	{
		return (_ColorLow * (1 - ((_SinTime.w + 1.0) / 2))) + (_ColorHigh * ((_SinTime.w + 1.0) / 2));

	//return fixed4(0, 0, 0, 0);
}
ENDCG
}
	}
}