﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayerAudio : MonoBehaviour
{
    [Header("Spawn Sounds")]
    [SerializeField] private AudioClip[] cultistClips;
    [SerializeField] private AudioClip[] builderClips;
    [SerializeField] private AudioClip[] bombClips;
    [SerializeField] private AudioClip[] tankClips;
    [SerializeField] private AudioClip[] ghostClips;
    [Header("Other sounds")]
    [SerializeField] private AudioClip[] voiceFailSounds;

    private float voiceCooldown = 3.5f;
    private float nextVoiceTime = 0f;
    private AudioSource audioSource;

    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    public void PlaySpawnSound(MinionType minion, float volume)
    {
        if (nextVoiceTime < Time.time)
        {
            audioSource.PlayOneShot(RandomClip(minion), volume);
            nextVoiceTime = Time.time + 3;
        }
    }

    public void PlayFailSound(float volume)
    {
        if (nextVoiceTime < Time.time)
        {
            audioSource.PlayOneShot(voiceFailSounds[Random.Range(0, voiceFailSounds.Length - 1)], volume);
            nextVoiceTime = Time.time + 3;
        }
    }

    private AudioClip RandomClip(MinionType minion)
    {
        AudioClip[] toChooseFrom = new AudioClip[1];
        switch (minion)
        {
            case MinionType.Cultist:
                toChooseFrom = cultistClips;
                break;
            case MinionType.Builder:
                toChooseFrom = builderClips;
                break;
            case MinionType.Bomb:
                toChooseFrom = bombClips;
                break;
            case MinionType.Tank:
                toChooseFrom = tankClips;
                break;
            case MinionType.Ghost:
                toChooseFrom = ghostClips;
                break;
        }
        return toChooseFrom[Random.Range(0, toChooseFrom.Length - 1)];
    }
}
