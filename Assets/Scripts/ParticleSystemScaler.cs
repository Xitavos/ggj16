﻿using UnityEngine;
using System.Collections;

public class ParticleSystemScaler : MonoBehaviour
{
    [SerializeField] ParticleSystem particles;
    [SerializeField] float scale;
    [SerializeField] bool includeChildren;

	void Start ()
    {
        Scale(particles, scale, includeChildren);
    }

    void Scale (ParticleSystem particles, float scale, bool includeChildren = true)
    {
        ScaleSystem(particles, scale, false);
        if (includeChildren)
        {
            var children = particles.GetComponentsInChildren<ParticleSystem>();
            for (var i = children.Length; i-- > 0;)
            {
                if (children[i] == particles) { continue; }
                ScaleSystem(children[i], scale, true);
            }
        }
    }

    private static void ScaleSystem(ParticleSystem particles, float scale, bool scalePosition)
    {
        if (scalePosition) { particles.transform.localPosition *= scale; }

        particles.startSize *= scale;
        particles.gravityModifier *= scale;
        particles.startSpeed *= scale;

        /*if (options.shape)
        {
            var shape = particles.shape;
            shape.radius *= scale;
            shape.box = shape.box * scale;
        }*/
    }
}
