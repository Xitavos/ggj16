﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
class FlashUI : MonoBehaviour
{
    [SerializeField] private Sprite[] sprites;
    [SerializeField] private float secondsPerFrame = 0.1f;

    private Image image;

    void Start()
    {
        image = gameObject.GetComponent<Image>();
        image.sprite = sprites[0];
        StartCoroutine(Animate());
    }

    IEnumerator Animate()
    {
        for (int i = 1; i < sprites.Length; i++)
        {
            yield return new WaitForSeconds(secondsPerFrame);
            image.sprite = sprites[i];
        }
        Destroy(gameObject);
    }
}
