﻿using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public static class ComboStore
{
    public static Dictionary<MinionType, ComboPart[]> Combos = new Dictionary<MinionType, ComboPart[]>()
    {
        {
            MinionType.Cultist,
            new ComboPart[]
            {
                new ComboPart(ButtonType.A, false, false),
                new ComboPart(ButtonType.B, false, false),
                new ComboPart(ButtonType.A, false, false),
            }
        },
        {
            MinionType.Builder,
            new ComboPart[]
            {
                new ComboPart(ButtonType.X, false, false),
                new ComboPart(ButtonType.B, false, false),
                new ComboPart(ButtonType.Y, false, false),
            }
        },
        {
            MinionType.Bomb,
            new ComboPart[]
            {
                new ComboPart(ButtonType.A, false, false),
                new ComboPart(ButtonType.X, false, false),
                new ComboPart(ButtonType.Y, false, false),
                new ComboPart(ButtonType.B, false, false)
            }
        },
        {
            MinionType.Tank,
            new ComboPart[]
            {
                new ComboPart(ButtonType.A, false, false),
                new ComboPart(ButtonType.B, false, false),
                new ComboPart(ButtonType.A, false, false),
                new ComboPart(ButtonType.B, false, false),
                new ComboPart(ButtonType.X, false, false),
                new ComboPart(ButtonType.Y, false, false),
            }
        },
        {
            MinionType.Ghost,
            new ComboPart[]
            {
                new ComboPart(ButtonType.A, false, false),
                new ComboPart(ButtonType.Y, false, false),
                new ComboPart(ButtonType.B, false, false),
                new ComboPart(ButtonType.X, false, false),
                new ComboPart(ButtonType.A, false, false),
                new ComboPart(ButtonType.Y, false, false),
            }
        }
    };
}

public class Combo
{
    public ComboPart[] Parts;
    public Action Ritual;

    public Combo(Action ritual, params ComboPart[] parts)
    {
        this.Ritual = ritual;
        this.Parts = parts;
    }

    public static Combo RandomX(Action ritual, int numberOfParts)
    {
        ComboPart[] parts = new ComboPart[numberOfParts];
        for (int i = 0; i < numberOfParts; i++)
            parts[i] = ComboPart.RandomComboPart();
        return new Combo(ritual, parts);
    }
}

public class ComboPart
{
    public ButtonType Button;
    public bool LeftTrigger;
    public bool RightTrigger;

    public ComboPart(ButtonType button, bool leftTrigger, bool rightTrigger)
    {
        this.Button = button;
        this.LeftTrigger = leftTrigger;
        this.RightTrigger = rightTrigger;
    }

    public static ComboPart RandomComboPart()
    {
        ButtonType button = (ButtonType)UnityEngine.Random.Range(0, 3);
        bool leftTrigger = Random.Range(0f, 1f) > 0.95f;
        bool rightTrigger = Random.Range(0f, 1f) > 0.95f;
        return new ComboPart(button, leftTrigger, rightTrigger);
    }

    public bool CorrectInput(ButtonType button, bool leftTrigger, bool rightTrigger)
    {
        return (button == Button && leftTrigger == LeftTrigger && rightTrigger == RightTrigger);
    }

    public override string ToString()
    {
        return string.Format("{0} {1} {2}", Button, LeftTrigger, RightTrigger);
    }
}