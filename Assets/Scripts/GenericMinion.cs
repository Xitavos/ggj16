﻿using UnityEngine;
using System.Collections;

public class GenericMinion : MonoBehaviour
{
    [HideInInspector] public Health healthScript;

	[HideInInspector] public MinionMovement minionMovement;

    [HideInInspector] public int player;
    [HideInInspector] public Lane lane;

	void Start()
    {
        healthScript = GetComponentInChildren<Health>();
		minionMovement = GetComponent<MinionMovement>();
	}
	
	protected virtual void Update()
    {
        if (healthScript == null)
        {
            Die();
        }
    }

    protected virtual void Die()
    {
        Spawner.instance.RemoveFromList(this, player, lane);
        Destroy(gameObject);
    }
}
