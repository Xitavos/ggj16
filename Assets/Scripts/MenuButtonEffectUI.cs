﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class MenuButtonEffectUI : MonoBehaviour
{
    [SerializeField] private Sprite normal;
    [SerializeField] private Sprite hover;
    [SerializeField] private Sprite click;

    private Image image;

    void Start()
    {
        image = GetComponent<Image>();
    }

    public void SetNormal()
    {
        image.sprite = normal;
    }

    public void SetHover()
    {
        image.sprite = hover;
    }

    public void SetClicked()
    {
        image.sprite = click;
    }
}
