﻿using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightGlow : MonoBehaviour
{
    private Light light;

    [SerializeField] private float frequency = 1f;
    [SerializeField] private float magnitude = 1f;
    [SerializeField] private float min = 3f;

    void Start()
    {
        light = gameObject.GetComponent<Light>();
    }

    void Update()
    {
        light.intensity = min + magnitude + Mathf.Sin(Time.timeSinceLevelLoad * frequency) * magnitude;
    }
}
