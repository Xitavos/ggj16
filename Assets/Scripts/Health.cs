﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public Image HealthBar;
    public Image HealthBarBackground;
    public int StartingHealth = 100;
    public bool showBackgroundBar = true;
	[HideInInspector] public int direction;

    float currentHealth;
    float healthPercent;
    bool isDead;

    void Start ()
    {
        currentHealth = StartingHealth;
		Vector3 temp = gameObject.transform.localPosition;

        if (GetComponentInParent<GenericMinion>() != null)
        {
            if (GetComponentInParent<GenericMinion>().player == 0)
                direction = 1;
            else if (GetComponentInParent<GenericMinion>().player == 1)
                direction = -1;

            temp.z *= direction;
            gameObject.transform.localPosition = temp;
        }
	}

    void Update ()
    {
        if (showBackgroundBar == false) // Check if the background bar needs to be on or off
        {
            ShowHideBackgroundBar();
        }

        if (GetComponentInParent<GenericMinion>() != null)
            transform.rotation = Camera.main.transform.rotation;

	}

    public void UpdateHealthBar ()
    {
        healthPercent = currentHealth / StartingHealth;

        HealthBar.transform.localScale = new Vector3(healthPercent, 1, 1);
    }

    public void TakeDamage (int damage)
    {
        if (!isDead)
        {
            currentHealth -= damage;
            UpdateHealthBar();

            if (currentHealth <= 0)
            {
                Die();
				Destroy(gameObject);
            }
        }
    }

    public void TakeDamageBase(int damage)
    {
        currentHealth -= damage;
        UpdateHealthBar();
    }

    void Die ()
    {
        isDead = true;
        // Do whatever the object needs to do when dying
        //print("Dead");
    }

    void ShowHideBackgroundBar ()
    {
        if (showBackgroundBar == true)
        {
            HealthBarBackground.gameObject.SetActive(true);
        }
        else
        {
            HealthBarBackground.gameObject.SetActive(false);
        }
    }
}
