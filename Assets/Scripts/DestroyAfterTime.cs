﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour
{
    [SerializeField] float time = 10;

	void Start ()
    {
        Destroy(gameObject, time);
	}
}
