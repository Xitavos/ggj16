﻿using UnityEngine;
using System.Collections;

public class GhostAttack : MinionAttack
{

    protected override void Start()
    {
        minionMovement = GetComponent<MinionMovement>();
        genericMinion = GetComponent<GenericMinion>();

        StartCoroutine(wait(0.1f));

        genericMinion.player = player;
        genericMinion.lane = lane;

        Spawner.instance.RemoveFromList(genericMinion, player, lane);
    }

    protected override void Update()
    {

    }

    void OnTriggerEnter (Collider other)
    {
        GenericMinion collidedMinion = other.GetComponent<GenericMinion>();

        if (other.tag == "Minion" && collidedMinion != null && collidedMinion.player != player)
        {
            collidedMinion.healthScript.TakeDamage(Damage);
        }

        BaseController baseController = other.GetComponent<BaseController>();

        if (baseController != null && baseController.PlayerIndex != player)
        {
            Destroy(gameObject);
        }
    }
}
