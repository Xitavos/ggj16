﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XInputDotNetPure;

public class PlayerInput : MonoBehaviour
{
    public PlayerIndex playerIndex;

    [HideInInspector] public int SelectedLane;
    [HideInInspector] public MinionType SelectedMinionType;

    private Dictionary<MinionType, float> cooldowns = new Dictionary<MinionType, float>()
    {
        { MinionType.Cultist, 2f },
        { MinionType.Builder, 10f },
        { MinionType.Bomb, 10f },
        { MinionType.Tank, 5f },
        { MinionType.Ghost, 10f },
    };

    private Dictionary<MinionType, float> timeAvailable = new Dictionary<MinionType, float>();

    public Combo CurrentCombo;
    public ComboPart CurrentComboPart;
    
    private GamePadState gamePadState;
    private GamePadState prevPadState;
	Spawner spawner;

    private bool xOutDeadZone;
    private bool prevXOutDeadZone;
    private bool yOutDeadZone;
    private bool prevYOutDeadZone;

    private bool leftOutDeadZone;
    private bool prevLeftOutDeadZone;
    private bool rightOutDeadZone;
    private bool prevRightOutDeadZone;

    private float stickDeadZone = 0.3f;
    private float triggerDeadZone = 0.4f;

    private Combo[] possibleCombos;
    private bool inCombo = false;
    private int comboIndex;

    #region Events
    public event EventHandler<StartedArgs> Started;
    protected virtual void OnStarted(StartedArgs e)
    {
        EventHandler<StartedArgs> handler = Started;
        if (handler != null)
            handler(this, e);
    }

    public event EventHandler<SucceededArgs> Succeeded;
    protected virtual void OnSucceeded(SucceededArgs e)
    {
        EventHandler<SucceededArgs> handler = Succeeded;
        if (handler != null)
            handler(this, e);
    }

    public event EventHandler<CompletedArgs> Completed;
    protected virtual void OnCompleted(CompletedArgs e)
    {
        EventHandler<CompletedArgs> handler = Completed;
        if (handler != null)
            handler(this, e);
    }

    public event EventHandler<FailedArgs> Failed;
    protected virtual void OnFailed(FailedArgs e)
    {
        EventHandler<FailedArgs> handler = Failed;
        if (handler != null)
            handler(this, e);
    }

    public event EventHandler<SelectionChangedArgs> SelectionChanged;
    protected virtual void OnSelectionChanged(SelectionChangedArgs e)
    {
        EventHandler<SelectionChangedArgs> handler = SelectionChanged;
        if (handler != null)
            handler(this, e);
    }
    #endregion

    void Start()
    {
		spawner = FindObjectOfType<Spawner>();
        foreach (MinionType value in Enum.GetValues(typeof (MinionType)))
            timeAvailable[value] = Time.time;
        gamePadState = GamePad.GetState(playerIndex);
        xOutDeadZone = false;
        yOutDeadZone = false;
        leftOutDeadZone = false;
        rightOutDeadZone = false;
    }
	
	void Update()
	{
        #region Input variable stuff
        prevPadState = gamePadState;
        gamePadState = GamePad.GetState(playerIndex);

	    prevXOutDeadZone = xOutDeadZone;
	    xOutDeadZone = Mathf.Abs(gamePadState.ThumbSticks.Left.X) > stickDeadZone;
        prevYOutDeadZone = yOutDeadZone;
        yOutDeadZone = Mathf.Abs(gamePadState.ThumbSticks.Left.Y) > stickDeadZone;

        prevLeftOutDeadZone = leftOutDeadZone;
        leftOutDeadZone = gamePadState.Triggers.Left > triggerDeadZone;
        prevRightOutDeadZone = rightOutDeadZone;
        rightOutDeadZone = gamePadState.Triggers.Right > triggerDeadZone;

	    if (!inCombo)
	    {
	        int minionSwitch = 0;
	        if (IsButtonPressed(gamePadState.DPad.Left, prevPadState.DPad.Left) || InputHelper.PlayerKeyPressed(playerIndex, DirectionKeyType.Left))
	            minionSwitch = -1;
            else if (IsButtonPressed(gamePadState.DPad.Right, prevPadState.DPad.Right) || InputHelper.PlayerKeyPressed(playerIndex, DirectionKeyType.Right))
                minionSwitch = 1;
            else if (gamePadState.IsConnected)
                minionSwitch = MinionSwitch();
	        SelectedMinionType = Remap(SelectedMinionType, minionSwitch, 5);
	        if (minionSwitch != 0)
	            OnSelectionChanged(new SelectionChangedArgs { Direction = minionSwitch });

            if (IsButtonPressed(gamePadState.DPad.Up, prevPadState.DPad.Up) || InputHelper.PlayerKeyPressed(playerIndex, DirectionKeyType.Up))
                SelectedLane--;
            else if (IsButtonPressed(gamePadState.DPad.Down, prevPadState.DPad.Down) || InputHelper.PlayerKeyPressed(playerIndex, DirectionKeyType.Down))
                SelectedLane++;
            else if (gamePadState.IsConnected)
                SelectedLane += LaneSwitch();
            SelectedLane = Mathf.Clamp(SelectedLane, 0, 2);
        }

        #endregion

        #region Combo Handling
        if (!inCombo)
	    {
	        if (IsButtonPressed(gamePadState.Buttons.A, prevPadState.Buttons.A) ||
               (IsTriggerPressed(leftOutDeadZone, prevLeftOutDeadZone) && rightOutDeadZone) ||
               (leftOutDeadZone && IsTriggerPressed(rightOutDeadZone, prevRightOutDeadZone)) ||
               InputHelper.PlayerKeyPressed(playerIndex, FunctionKeyType.Select))
            {
                if (Time.time > timeAvailable[SelectedMinionType])
                {
                    inCombo = true;

                    CurrentCombo = new Combo(() => spawner.SpawnEnemy(SelectedMinionType, playerIndex == PlayerIndex.One ? 1 : -1, (Lane)SelectedLane), ComboStore.Combos[SelectedMinionType]);

                    comboIndex = 0;
                    CurrentComboPart = CurrentCombo.Parts[comboIndex];
                    OnStarted(new StartedArgs { NewCombo = CurrentCombo });
                }
            }
	    }
	    else
	    {
	        bool leftTrigger = gamePadState.Triggers.Left > triggerDeadZone;
            bool rightTrigger = gamePadState.Triggers.Right > triggerDeadZone;
            bool attempted = false;
            bool succeeded = false;
	        if (IsButtonPressed(gamePadState.Buttons.A, prevPadState.Buttons.A) || InputHelper.PlayerKeyPressed(playerIndex, ButtonType.A))
	        {
	            attempted = true;
                succeeded = CurrentComboPart.CorrectInput(ButtonType.A, leftTrigger, rightTrigger);
            }
            else if (IsButtonPressed(gamePadState.Buttons.B, prevPadState.Buttons.B) || InputHelper.PlayerKeyPressed(playerIndex, ButtonType.B))
            {
                attempted = true;
                succeeded = CurrentComboPart.CorrectInput(ButtonType.B, leftTrigger, rightTrigger);
            }
            else if (IsButtonPressed(gamePadState.Buttons.X, prevPadState.Buttons.X) || InputHelper.PlayerKeyPressed(playerIndex, ButtonType.X))
            {
                attempted = true;
                succeeded = CurrentComboPart.CorrectInput(ButtonType.X, leftTrigger, rightTrigger);
            }
            else if (IsButtonPressed(gamePadState.Buttons.Y, prevPadState.Buttons.Y) || InputHelper.PlayerKeyPressed(playerIndex, ButtonType.Y))
            {
                attempted = true;
                succeeded = CurrentComboPart.CorrectInput(ButtonType.Y, leftTrigger, rightTrigger);
            }

	        if (attempted)
	        {
	            if (succeeded)
	            {
	                comboIndex++;
	                if (comboIndex == CurrentCombo.Parts.Length)
	                {
	                    inCombo = false;
	                    CurrentCombo.Ritual();
                        OnCompleted(new CompletedArgs { StartTime = Time.time, EndTime = Time.time + cooldowns[SelectedMinionType] });
	                    timeAvailable[SelectedMinionType] = Time.time + cooldowns[SelectedMinionType];
	                }
	                else
                    {
                        CurrentComboPart = CurrentCombo.Parts[comboIndex];
                        OnSucceeded(new SucceededArgs());
                    }
	            }
	            else
	            {
	                inCombo = false;
	                OnFailed(new FailedArgs { Player = playerIndex, StartTime = Time.time, EndTime = Time.time + cooldowns[SelectedMinionType] });
                    timeAvailable[SelectedMinionType] = Time.time + cooldowns[SelectedMinionType];
                }
	        }
        }
        #endregion
    }

    private MinionType Remap(MinionType minionType, int minionSwitch, int max)
    {
        int enumAsInt = (int)minionType;
        enumAsInt += minionSwitch;
        if (enumAsInt < 0)
            enumAsInt = max + enumAsInt;
        else if (enumAsInt >= max)
            enumAsInt = 0 + (max - enumAsInt);
        return (MinionType)enumAsInt;
    }

    private int MinionSwitch()
    {
        if (xOutDeadZone && !prevXOutDeadZone)
            return (int)Mathf.Sign(gamePadState.ThumbSticks.Left.X);
         return 0;
    }

    private int LaneSwitch()
    {
        if (yOutDeadZone && !prevYOutDeadZone)
            return -(int)Mathf.Sign(gamePadState.ThumbSticks.Left.Y);
        return 0;
    }

    bool IsButtonPressed(ButtonState buttonState, ButtonState prevButtonState)
    {
        return buttonState == ButtonState.Pressed && prevButtonState == ButtonState.Released;
    }

    bool IsTriggerPressed(bool trigger, bool prevTrigger)
    {
        return trigger && !prevTrigger;
    }

    bool IsButtonReleased(ButtonState buttonState, ButtonState prevButtonState)
    {
        return buttonState == ButtonState.Released && prevButtonState == ButtonState.Pressed;
    }
}

#region Event Args
public class StartedArgs : EventArgs
{
    public Combo NewCombo { get; set; }
}

public class SucceededArgs : EventArgs
{
    
}

public class CompletedArgs : EventArgs
{
    public float StartTime { get; set; }
    public float EndTime { get; set; }
}

public class FailedArgs : EventArgs
{
    public PlayerIndex Player { get; set; }
    public float StartTime { get; set; }
    public float EndTime { get; set; }
}

public class SelectionChangedArgs : EventArgs
{
    public int Direction { get; set; }
}
#endregion