﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using XInputDotNetPure;

public class CreditsInput : MonoBehaviour
{
    public GamePadState GamePadState;
    private GamePadState prevPadState;

    void Start()
    {
        GamePadState = GamePad.GetState(PlayerIndex.One);
    }

    void Update()
    {
        prevPadState = GamePadState;
        GamePadState = GamePad.GetState(PlayerIndex.One);
        if (IsButtonPressed(GamePadState.Buttons.A, prevPadState.Buttons.A) || InputHelper.PlayerKeyPressed(PlayerIndex.One, FunctionKeyType.Select))
            SceneManager.LoadScene("Main Menu");
    }

    bool IsButtonPressed(ButtonState buttonState, ButtonState prevButtonState)
    {
        return buttonState == ButtonState.Pressed && prevButtonState == ButtonState.Released;
    }
}
