﻿public enum MinionType
{
    Cultist = 0,
    Builder = 1,
    Bomb = 2,
    Tank = 3,
    Ghost = 4
}

public enum ButtonType
{
    A = 0,
    B = 1,
    X = 2,
    Y = 3
}

public enum DirectionKeyType
{
    Up = 0,
    Down = 1,
    Left = 2,
    Right = 3
}

public enum FunctionKeyType
{
    Select = 0
}

public enum Lane
{
	Top = 0,
	Middle = 1,
	Bottom = 2
}