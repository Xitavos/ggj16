﻿using UnityEngine;
using System;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
[AddComponentMenu("Image Effects/Outline")]

public class ToonOutline : UnityStandardAssets.ImageEffects.PostEffectsBase {

    public Shader toonOutlineShader;
    private Material toonOutlineMat = null;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    [ImageEffectOpaque]
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        toonOutlineMat = CheckShaderAndCreateMaterial(toonOutlineShader, toonOutlineMat);

        Graphics.Blit(source, destination, toonOutlineMat);
    }

}
