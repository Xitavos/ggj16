﻿using UnityEngine;
using UnityEngine.UI;

public class MenuTextUI : MonoBehaviour
{
    [SerializeField] private Sprite[] sprites;

    private Image image;

    void Start()
    {
        image = gameObject.GetComponent<Image>();
        image.sprite = sprites[0];
        gameObject.transform.localScale = Vector3.one * 1.5f;
        gameObject.transform.localPosition = new Vector3(0f, 150f);
    }

    public void SetImage(int index)
    {
        image.sprite = sprites[index];
        image.SetNativeSize();
        if (index == 0)
        {
            gameObject.transform.localScale = Vector3.one * 1.5f;
            gameObject.transform.localPosition = new Vector3(0f, 150f);
        }
        else
        {
            gameObject.transform.localScale = Vector3.one;
            gameObject.transform.localPosition = new Vector3(0f, 200f);
        }
    }
}
