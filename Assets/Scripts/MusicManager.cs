﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class MusicManager : MonoBehaviour
{
	[SerializeField] AudioMixer musicMixer;
	[SerializeField] AudioMixerSnapshot[] snapshots;
	[SerializeField] float timeToTransition = 10f;

	[SerializeField] BaseController[] basecontrollers;

	int maxSumHealth;
	int currentSnapshotIndex;

	MusicState musicState;
	enum MusicState
	{
		Low,
		Medium, 
		Intense
	}

	void Start()
	{
		StartCoroutine(UpdateAudio());
		maxSumHealth = basecontrollers[0].InitialHealth * basecontrollers.Length;
	}

	IEnumerator UpdateAudio()
	{
		while (true)
		{
			int baseHealth = 0;

			for (int i = 0; i < basecontrollers.Length; i++)
			{
				baseHealth += basecontrollers[i].CurrentHealth;
			}

			switch (musicState)
			{
				case MusicState.Low:
					if (baseHealth / (float)maxSumHealth < 0.75f)
					{
						musicState = MusicState.Medium;
						Medium();
					}
					break;
				case MusicState.Medium:
					if (baseHealth / (float)maxSumHealth < 0.5f)
					{
						musicState = MusicState.Intense;
						Big();
					}
					break;
				case MusicState.Intense:
					break;
			}

			yield return new WaitForSeconds(1f);
		}
	}

	[ContextMenu("TEST")]
	void Pause()
	{
		if (currentSnapshotIndex == 0)
		{
			musicMixer.SetFloat("LowpassCutoff", 2000);
			musicMixer.SetFloat("HighpassCutoff", 4000);
			currentSnapshotIndex = 1;
		}
		else if (currentSnapshotIndex == 1)
		{
			musicMixer.SetFloat("LowpassCutoff", 10);
			musicMixer.SetFloat("HighpassCutoff", 22000);
			currentSnapshotIndex = 0;
		}
	}

	[ContextMenu("SMALL")]
	void Small()
	{
		snapshots[0].TransitionTo(timeToTransition);
	}

	[ContextMenu("MEDIUM")]
	void Medium()
	{
		snapshots[1].TransitionTo(timeToTransition);
	}

	[ContextMenu("BIG")]
	void Big()
	{
		snapshots[2].TransitionTo(timeToTransition);
	}
}
