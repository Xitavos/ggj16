﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraWiggle : MonoBehaviour
{
	[SerializeField] float smoothing = 1f;
	[SerializeField] float maxXRotation = 5f;
	[SerializeField] Transform targetTransform;

	float targetAngle;

	float[] minionXPositions;
	Spawner spawner;

	void Start()
	{
		minionXPositions = new float[6];
		spawner = FindObjectOfType<Spawner>();
		StartCoroutine(UpdateRotation());
	}

	void Update()
	{
		transform.rotation = Quaternion.Lerp(transform.rotation, targetTransform.rotation, Time.deltaTime * smoothing);
	}

	IEnumerator UpdateRotation()
	{
		while (true)
		{
			List<GenericMinion>[] playerOneLanes = spawner.playerOneLanes;
			List<GenericMinion>[] playerTwoLanes = spawner.playerTwoLanes;

			int index = 0;

			foreach (List<GenericMinion> item in playerOneLanes)
			{
				if (item.Count <= 0)
				{
					minionXPositions[index] = 0;
					continue;
				}
				if (item[0] == null)
				{
					minionXPositions[index] = 0;
					continue;
				}
				//else
				{
					minionXPositions[index] = item[0].transform.position.x;
				}
				index++;
			}
			foreach (List<GenericMinion> item in playerTwoLanes)
			{
				if (item.Count <= 0)
				{
					minionXPositions[index] = 0;
					continue;
				}
				if (item[0] == null)
				{
					minionXPositions[index] = 0;
					continue;
				}
				//else
				{
					minionXPositions[index] = item[0].transform.position.x;
				}
				index++;
			}

			float averageXPosition = 0;
			for (int i = 0; i < minionXPositions.Length; i++)
			{
				averageXPosition += minionXPositions[i];
			}
			averageXPosition /= minionXPositions.Length;

			averageXPosition = Mathf.Clamp(averageXPosition, -maxXRotation, maxXRotation);

			Vector3 direction = (new Vector3(averageXPosition, 0, 0) - transform.position).normalized;

			targetTransform.transform.forward = direction;

			yield return new WaitForSeconds(1f);
		}
	}
}
