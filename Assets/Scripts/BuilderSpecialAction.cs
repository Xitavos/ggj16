﻿using UnityEngine;
using System.Collections;

public class BuilderSpecialAction : GenericSpecialAction {
	[SerializeField] GameObject towerPrefab;

	public override void ReachedTargetAction(Transform location, int ID)
	{
        if (location.FindChild("Tower(Clone)") == null && location.tag == "TowerLocation")
        {
            GenericMinion genericMinion = GetComponent<GenericMinion>();
            GameObject newTower = Instantiate(towerPrefab) as GameObject;
			newTower.transform.parent = location;
			newTower.transform.localPosition = new Vector3(0, 0, -1);
            int player = GetComponent<MinionAttack>().player;
		    Tower towerComponent = newTower.GetComponent<Tower>();
            towerComponent.belongsToPlayer = GetComponent<GenericMinion>().player;
            towerComponent.FaceToNext(genericMinion.lane, genericMinion.player);
            Destroy(gameObject);
		}
	}
}
