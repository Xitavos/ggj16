﻿using System;
using UnityEngine;
using XInputDotNetPure;

public class PlayerComboUI : MonoBehaviour
{
    [SerializeField] private GameObject playerInputObject;
    [SerializeField] private GameObject buttonPrefab;
    [SerializeField] private GameObject flashPrefab;
    [SerializeField] private GameObject arrow;
    [SerializeField] private int inputsToDisplay = 4;
	[SerializeField] AudioClip[] failsounds;
	[SerializeField] AudioClip[] succeedsounds;

    [SerializeField] private GameObject playerVoice;

    private PlayerAudio playerAudio;

    private GameObject[] displayedInputs;
    private ComboPart[] inputs;
    private PlayerInput playerInput;
    private int currentIndex = 0;
    private float spacing = 96f;
	SoundEffectsManager soundEffectsManager;


    void Start()
    {
        playerInput = playerInputObject.GetComponent<PlayerInput>();
        playerInput.Started += OnStarted;
        playerInput.Succeeded += OnSucceeded;
        playerInput.Completed += OnCompleted;
        playerInput.Failed += OnFailed;
        displayedInputs = new GameObject[inputsToDisplay];
        arrow.SetActive(false);
		soundEffectsManager = FindObjectOfType<SoundEffectsManager>();
        playerAudio = playerVoice.GetComponent<PlayerAudio>();
    }

    private void OnStarted(object sender, StartedArgs e)
    {
        inputs = e.NewCombo.Parts;
        for (int i = 0; i < inputsToDisplay; i++)
            displayedInputs[i] = CreateComboPart(inputs[i], i);
        arrow.SetActive(true);
    }
    
    private void OnFailed(object sender, FailedArgs e)
    {
		soundEffectsManager.PlaySound(Camera.main.transform.position, failsounds);
        playerAudio.PlayFailSound(1f);
        Clear();
    }

    private void OnCompleted(object sender, CompletedArgs e)
    {
		Poof();
        Clear();
		soundEffectsManager.PlaySound(Camera.main.transform.position, succeedsounds);
	}

    private void Poof()
    {
        GameObject newFlash = Instantiate(flashPrefab) as GameObject;
        newFlash.transform.SetParent(gameObject.transform);
        newFlash.transform.localPosition = Vector3.zero;
    }

    private void Clear()
    {
        arrow.SetActive(false);
        currentIndex = 0;
        foreach (GameObject displayedInput in displayedInputs)
            Destroy(displayedInput);
        inputs = null;
        displayedInputs = new GameObject[inputsToDisplay];
    }

    private void OnSucceeded(object sender, SucceededArgs e)
    {
        Poof();
        ArrowBoof();
        GameObject temp = displayedInputs[0];
        displayedInputs[0] = null;
        Destroy(temp);
        for (int i = 0; i < inputsToDisplay - 1; i++)
        {
            displayedInputs[i] = displayedInputs[i + 1];
            displayedInputs[i].transform.localPosition = new Vector3(0, i * spacing);
        }
        currentIndex++;
        //BEGIN SPECIAL CASE
        if (currentIndex == inputs.Length - 1)
            displayedInputs[0].transform.localPosition = new Vector3(0, 0);
        //END SPECIAL CASE
        if (currentIndex + inputsToDisplay <= inputs.Length)
            displayedInputs[inputsToDisplay - 1] = CreateComboPart(inputs[currentIndex + inputsToDisplay - 1], inputsToDisplay - 1);

		soundEffectsManager.PlaySound(Camera.main.transform.position, succeedsounds);
	}

    void ArrowBoof()
    {
        arrow.transform.localScale = new Vector3(2f, 2f);
    }

    void Update()
    {
        arrow.transform.localScale = Vector3.Lerp(arrow.transform.localScale, Vector3.one, Time.deltaTime * 10f);
    }

    private GameObject CreateComboPart(ComboPart c, int offset)
    {
        GameObject button = Instantiate(buttonPrefab);
        button.transform.SetParent(gameObject.transform);
        button.transform.localPosition = new Vector3(0, offset * spacing);
        button.transform.localScale = Vector3.one;
        button.GetComponent<ButtonUI>().Initialize(c, playerInput.playerIndex);
        return button;
    }
}
