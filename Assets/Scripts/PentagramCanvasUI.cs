﻿using System.Collections.Generic;
using UnityEngine;

class PentagramCanvasUI : MonoBehaviour
{
    [SerializeField] private GameObject[] markers;
    [SerializeField] private GameObject playerInputObject;

    private List<Vector3> positions = new List<Vector3>();
    private PlayerInput playerInput;

    void Start()
    {
        foreach (GameObject marker in markers)
            positions.Add(marker.transform.position + Vector3.up * 2f);
        playerInput = playerInputObject.GetComponent<PlayerInput>();
    }

    void Update()
    {
        // Billboarding
        transform.rotation = Camera.main.transform.rotation;

        // Shift position
        transform.position = positions[playerInput.SelectedLane];
    }
}
