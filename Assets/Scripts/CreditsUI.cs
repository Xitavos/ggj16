﻿using UnityEngine;

public class CreditsUI : MonoBehaviour
{
    private CreditsInput input;

    void Start()
    {
        input = GameObject.Find("Credits Input").GetComponent<CreditsInput>();
    }

	void Update()
	{
	    if (Mathf.Abs(input.GamePadState.ThumbSticks.Left.Y) > 0.3f)
	        transform.localPosition += Vector3.up * 5f * -input.GamePadState.ThumbSticks.Left.Y;
        else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            transform.localPosition += Vector3.up * 5f;
        else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            transform.localPosition += Vector3.down * 5f;
        else
            transform.localPosition += Vector3.up * 2.5f;
    }
}
