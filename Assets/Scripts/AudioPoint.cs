﻿using UnityEngine;
using System.Collections;

public class AudioPoint : MonoBehaviour
{
	AudioSource audiosource;

	[HideInInspector] public AudioClip Clip;

	void Start()
	{
		audiosource = GetComponent<AudioSource>();
		AudioClip clip = Clip;

		audiosource.clip = Clip;
		audiosource.Play();
		Destroy(gameObject, clip.length);	
	}
}
