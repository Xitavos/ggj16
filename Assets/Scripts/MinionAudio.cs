﻿using UnityEngine;
using System.Collections;

public class MinionAudio : MonoBehaviour
{
	[SerializeField] AudioClip[] bombSounds;
	[SerializeField] AudioClip[] builderSounds;
	[SerializeField] AudioClip[] cultistSounds;
	[SerializeField] AudioClip[] tankSounds;
	SoundEffectsManager soundEffectsManager;

	AudioClip[] mySounds;

	[SerializeField] float minimumTimeBetweenSounds = 15f;
	[SerializeField] float maximumTimeBetweenSounds = 30f;

	void Start()
	{
		string myName = gameObject.name;
		soundEffectsManager = FindObjectOfType<SoundEffectsManager>();

		switch (myName)
		{
			case "Bomb(Clone)":
				mySounds = bombSounds;
				break;
			case "Builder(Clone)":
				mySounds = builderSounds;
				break;
			case "Cultist(Clone)":
				mySounds = cultistSounds;
				break;
			case "Tank(Clone)":
				mySounds = tankSounds;
				break;
			default:
				break;
		}

		StartCoroutine(SoundLoop());
	}

	IEnumerator SoundLoop()
	{
		while (true)
		{
			float amountOfSecondsBetweenLoop = Random.Range(minimumTimeBetweenSounds, maximumTimeBetweenSounds);
			yield return new WaitForSeconds(amountOfSecondsBetweenLoop);
			soundEffectsManager.PlaySound(transform.position, mySounds);
		}
	}
}
