﻿using System.Collections;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using XInputDotNetPure;

public class MainMenuButtonUI : MonoBehaviour
{
    [SerializeField] private GameObject textObject;
    [SerializeField] private GameObject[] buttons;
    [SerializeField] private MenuButtonEffectUI[] buttonEffects;

    private GamePadState gamePadState;
    private GamePadState prevPadState;
    private bool xOutDeadZone;
    private bool prevXOutDeadZone;
    private float stickDeadZone = 0.3f;

    private Vector3[] buttonPositions;
    private MenuTextUI textUI;
    private int currentIndex = 0;
    private int maxIndex = 3;
    private bool clicked = false;

    void Start()
    {
        //Setup
        xOutDeadZone = false;
        gamePadState = GamePad.GetState(PlayerIndex.One);
        buttonPositions = buttons.Select(g => g.transform.localPosition).ToArray();
        buttonEffects = buttons.Select(g => g.GetComponent<MenuButtonEffectUI>()).ToArray();
        textUI = textObject.GetComponent<MenuTextUI>();
    }

    void Update()
    {
        //Input
        prevPadState = gamePadState;
        gamePadState = GamePad.GetState(PlayerIndex.One);
        prevXOutDeadZone = xOutDeadZone;
        xOutDeadZone = Mathf.Abs(gamePadState.ThumbSticks.Left.X) > stickDeadZone;

        int buttonSwitch = 0;
        if (IsButtonPressed(gamePadState.DPad.Left, prevPadState.DPad.Left) || InputHelper.PlayerKeyPressed(PlayerIndex.One, DirectionKeyType.Left))
            buttonSwitch = -1;
        else if (IsButtonPressed(gamePadState.DPad.Right, prevPadState.DPad.Right) || InputHelper.PlayerKeyPressed(PlayerIndex.One, DirectionKeyType.Right))
            buttonSwitch = 1;
        else if (gamePadState.IsConnected)
            buttonSwitch = ButtonSwitch();

        //Index changing
        currentIndex += buttonSwitch;
        if (currentIndex < 0)
            currentIndex = maxIndex;
        else if (currentIndex > maxIndex)
            currentIndex = 0;

        if (buttonSwitch == 1)
        {
            RotateRight();
            ResetText();
        }
        else if (buttonSwitch == -1)
        {
            RotateLeft();
            ResetText();
        }

        //Display
        if (!clicked)
        {
            foreach (MenuButtonEffectUI buttonEffect in buttonEffects)
                buttonEffect.SetNormal();
            buttonEffects[currentIndex].SetHover();
        }
        
        //Choosing
        if (IsButtonPressed(gamePadState.Buttons.A, prevPadState.Buttons.A))
        {
            PlayerPrefs.SetString("UI", "Controller");
            StartCoroutine(PerformFunction());
        }
        else if (InputHelper.PlayerKeyPressed(PlayerIndex.One, FunctionKeyType.Select))
        {
            PlayerPrefs.SetString("UI", "Keyboard");
            StartCoroutine(PerformFunction());
        }

        //Lerping
        for (int i = 0; i <= maxIndex; i++)
            buttons[i].transform.localPosition = Vector3.Lerp(buttons[i].transform.localPosition, buttonPositions[i], Time.deltaTime * 10f);
    }

    bool IsButtonPressed(ButtonState buttonState, ButtonState prevButtonState)
    {
        return buttonState == ButtonState.Pressed && prevButtonState == ButtonState.Released;
    }

    void ResetText()
    {
        textUI.SetImage(currentIndex);
    }

    IEnumerator PerformFunction()
    {
        clicked = true;
        float endTime = Time.time + 0.2f;
        while (Time.time < endTime)
        {
            buttons[0].transform.localScale = Vector3.Lerp(buttons[0].transform.localScale, Vector3.one * 0.75f, Time.deltaTime * 15f);
            yield return null;
        }
        switch (currentIndex)
        {
            case 0: //Play
                SceneManager.LoadScene("Main");
                break;
            case 1: //Help
                if (PlayerPrefs.GetString("UI") == "Controller")
                    SceneManager.LoadScene("Help");
                else
                    SceneManager.LoadScene("Help Keyboard");
                break;
            case 2: //Quit
                Application.Quit();
                break;
            case 3: //Credits
                SceneManager.LoadScene("Credits");
                break;
        }
    }

    private int ButtonSwitch()
    {
        if (xOutDeadZone && !prevXOutDeadZone)
            return (int)Mathf.Sign(gamePadState.ThumbSticks.Left.X);
        return 0;
    }

    private void RotateLeft()
    {
        GameObject last = buttons[maxIndex];
        for (int i = maxIndex; i > 0; i--)
            buttons[i] = buttons[i - 1];
        buttons[0] = last;
    }

    private void RotateRight()
    {
        GameObject first = buttons[0];
        for (int i = 0; i < maxIndex; i++)
            buttons[i] = buttons[i + 1];
        buttons[maxIndex] = first;
    }
}
