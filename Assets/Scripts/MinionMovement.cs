﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MinionMovement : MonoBehaviour
{
	NavMeshAgent pathfinder;

	[SerializeField] float speed;
	[SerializeField] float followDistance;

	[SerializeField] int[] topPath;
	[SerializeField] int[] middlePath;
	[SerializeField] int[] bottomPath;

	[HideInInspector] public Lane PathEnum;

	[HideInInspector] public bool ReversePath;

	[HideInInspector] public bool canMove;

	[SerializeField] Transform[] path;
	Rigidbody rigidbody;
	Vector3 oldDirection;
	float currentSpeed;

	int targetIndex = 1;
	Transform target;

	List<GenericMinion> myList;
	GenericMinion genericMinion;

	void Start()
	{
		canMove = true;
		currentSpeed = speed;

		rigidbody = GetComponent<Rigidbody>();
		myList = ReversePath ? Spawner.instance.playerTwoLanes[(int)PathEnum] : Spawner.instance.playerOneLanes[(int)PathEnum];
		genericMinion = GetComponent<GenericMinion>();

		int[] pathArray = null;

		switch (PathEnum)
		{
			case Lane.Top:
				pathArray = topPath;
				break;
			case Lane.Middle:
				pathArray = middlePath;
				break;
			case Lane.Bottom:
				pathArray = bottomPath;
				break;
		}
		Transform[] wayPoints = FindObjectOfType<Waypoints>().WaypointsArray;
		path = new Transform[pathArray.Length];

		for (int i = 0; i < pathArray.Length; i++)
		{
			path[i] = wayPoints[pathArray[i]];
		}

		//Should we reverse the path
		if (ReversePath)
		{
			Array.Reverse(path);
		}

		target = path[targetIndex];
	}

	void Update()
	{
        if (gameObject.name != "Ghost(Clone)")
        {
            DoFriendlyTrains();
        }

		if (targetIndex == path.Length)//If we are at the end of our path
		{
			rigidbody.velocity = Vector3.zero;
			return;
		}

		Vector3 targetDirection = target.position - transform.position;
		Vector3 direction = Vector3.Lerp(oldDirection, targetDirection, Time.deltaTime * 0.3f);

		direction = direction.normalized;

		if (canMove)
		{
			rigidbody.velocity = direction * currentSpeed;
		}
		else
		{
			rigidbody.velocity = Vector3.zero;
		}

		if (Vector2.Distance(transform.position, target.position) < 1.2f)
		{
			//Do an action based on minion type, as we have reached a target.
			GenericSpecialAction specialActionComponent;
			specialActionComponent = GetComponent<GenericSpecialAction>();
			if (specialActionComponent != null)
			{
				//100% beautiful, flawless code. Passes the target location we've reached and its ID.
				specialActionComponent.ReachedTargetAction(target, int.Parse(System.Text.RegularExpressions.Regex.Replace(target.name, "[^0-9.]", "")));
			}

			targetIndex++;

			if (targetIndex != path.Length)//If we are not at the end of our path
			{
				target = path[targetIndex];
			}
		}

		RotateTheModel(direction);

		oldDirection = direction;
	}

    void LateUpdate ()
    {
        if (gameObject.name == "Ghost(Clone)")
        {


            transform.position = transform.position + Vector3.up * Mathf.Sin(Time.time * speed) * 0.02f + Vector3.up * 0.01f;
        }
    }

	private void RotateTheModel(Vector3 direction)
	{
		transform.forward = direction.normalized;
	}

	private void DoFriendlyTrains()
	{
        if (canMove)
        {
            int inFrontIndex = myList.IndexOf(genericMinion) - 1;
            if (inFrontIndex == -1)
            {
                // You're the front of the line. You're the caboose! :D
                return;
            }

            // Apparently use sqrMagnitude is faster so we're gonna multiply rather than sqrting
            if (myList[inFrontIndex] != null && (myList[inFrontIndex].transform.position - transform.position).sqrMagnitude < followDistance * followDistance)
            {
                canMove = myList[inFrontIndex].minionMovement.canMove;
                currentSpeed = myList[inFrontIndex].minionMovement.currentSpeed;
            }
            else
            {
                currentSpeed = speed;
            }
        }
	}
}
