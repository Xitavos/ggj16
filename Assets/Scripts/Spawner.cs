﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
	public static Spawner instance;

	//[HideInInspector]
	public List<GenericMinion>[] playerOneLanes =
	{
		new List<GenericMinion>(),  // Top lane
		new List<GenericMinion>(),  // Middle lane
		new List<GenericMinion>()   // Bottom lane
	};

	//[HideInInspector]
	public List<GenericMinion>[] playerTwoLanes =
	{
		new List<GenericMinion>(),  // Top lane
		new List<GenericMinion>(),  // Middle lane
		new List<GenericMinion>()   // Bottom lane
	};

    [Header("Minions")]
	[SerializeField] GameObject cultist;
	[SerializeField] GameObject builder;
	[SerializeField] GameObject bomb;
	[SerializeField] GameObject tank;
    [SerializeField] GameObject ghost;

    [Header("Positions")]
	[SerializeField] Transform leftLaneSpawnPoint;
	[SerializeField] Transform rightLaneSpawnPoint;
    [SerializeField] private GameObject playerOneVoice;
    [SerializeField] private GameObject playerTwoVoice;

    private PlayerAudio playerOneAudio;
    private PlayerAudio playerTwoAudio;
	
	[SerializeField] MinionType qMinionType;
	[SerializeField] int qDirection;
	[SerializeField] Lane qLane;

	public void Start()
	{
		instance = this;
		
		playerOneAudio = playerOneVoice.GetComponent<PlayerAudio>();
		playerTwoAudio = playerTwoVoice.GetComponent<PlayerAudio>();

        //SpawnEnemy(MinionType.Cultist, -1, Lane.Middle);
    }
	
	[ContextMenu("Spawn Minion")]
	void SpawnEnemyFromInspector()
	{
		SpawnEnemy(qMinionType, qDirection, qLane);
	}

    public void SpawnEnemy(MinionType minion, int direction, Lane lane)
	{
		GameObject minionToSpawn = null;
		Transform spawnPoint = direction == -1 ? leftLaneSpawnPoint : rightLaneSpawnPoint;

		switch (minion)
		{
			case MinionType.Cultist:
				minionToSpawn = cultist;
				break;
			case MinionType.Builder:
				minionToSpawn = builder;
				break;
			case MinionType.Bomb:
				minionToSpawn = bomb;
				break;
			case MinionType.Tank:
				minionToSpawn = tank;
				break;
             case MinionType.Ghost:
                minionToSpawn = ghost;
		        break;
		}
        //spawnPoint.rotation
        GameObject newMinion = Instantiate(minionToSpawn, spawnPoint.position, spawnPoint.rotation) as GameObject;
		MinionMovement minionMovement = newMinion.GetComponent<MinionMovement>();
		minionMovement.PathEnum = lane;
		bool reversePath = direction == -1;
		minionMovement.ReversePath = reversePath;

		GenericMinion genericMinion = newMinion.GetComponent<GenericMinion>();

		MinionAttack minionAttack = genericMinion.GetComponent<MinionAttack>();

		if (minionAttack != null)
		{
			minionAttack.lane = lane;
			minionAttack.player = reversePath ? 1 : 0;
		}

        if (qMinionType != MinionType.Ghost)
        {
            //Add to the lists
            if (!reversePath)
            {
                playerOneLanes[(int)lane].Add(genericMinion);
            }
            else
            {
                playerTwoLanes[(int)lane].Add(genericMinion);
            }
        }

        PlaySpawnSound(minion, direction);
	}

    public void PlaySpawnSound(MinionType minion, int direction)
    {
        (direction == 1 ? playerOneAudio : playerTwoAudio).PlaySpawnSound(minion, 1f);
    }

	public void RemoveFromList (GenericMinion toRemove, int player, Lane lane)
	{
		//myList = player == 1 ? playerOneLanes[(int)lane] : playerTwoLanes[(int)lane];

		if (player == 0)
			playerOneLanes[(int)lane].Remove(toRemove);
		else
			playerTwoLanes[(int)lane].Remove(toRemove);
	}
}
