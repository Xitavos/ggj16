﻿using UnityEngine;
using XInputDotNetPure;

public static class InputHelper
{
    public static bool PlayerKeyPressed(PlayerIndex playerIndex, ButtonType buttonType)
    {
        KeyCode key = KeyCode.Alpha0; //Default value, shouldn't cause issues
        switch (playerIndex)
        {
            case PlayerIndex.One:
                switch (buttonType)
                {
                    case ButtonType.A:
                        key = KeyCode.Alpha1;
                        break;
                    case ButtonType.B:
                        key = KeyCode.Alpha2;
                        break;
                    case ButtonType.X:
                        key = KeyCode.Alpha3;
                        break;
                    case ButtonType.Y:
                        key = KeyCode.Alpha4;
                        break;
                }
                break;
            case PlayerIndex.Two:
                switch (buttonType)
                {
                    case ButtonType.A:
                        key = KeyCode.U;
                        break;
                    case ButtonType.B:
                        key = KeyCode.I;
                        break;
                    case ButtonType.X:
                        key = KeyCode.O;
                        break;
                    case ButtonType.Y:
                        key = KeyCode.P;
                        break;
                }
                break;
        }
        return Input.GetKeyDown(key);
    }

    public static bool PlayerKeyPressed(PlayerIndex playerIndex, DirectionKeyType buttonType)
    {
        KeyCode key = KeyCode.Alpha0; //Default value, shouldn't cause issues
        switch (playerIndex)
        {
            case PlayerIndex.One:
                switch (buttonType)
                {
                    case DirectionKeyType.Up:
                        key = KeyCode.W;
                        break;
                    case DirectionKeyType.Down:
                        key = KeyCode.S;
                        break;
                    case DirectionKeyType.Left:
                        key = KeyCode.A;
                        break;
                    case DirectionKeyType.Right:
                        key = KeyCode.D;
                        break;
                }
                break;
            case PlayerIndex.Two:
                switch (buttonType)
                {
                    case DirectionKeyType.Up:
                        key = KeyCode.UpArrow;
                        break;
                    case DirectionKeyType.Down:
                        key = KeyCode.DownArrow;
                        break;
                    case DirectionKeyType.Left:
                        key = KeyCode.LeftArrow;
                        break;
                    case DirectionKeyType.Right:
                        key = KeyCode.RightArrow;
                        break;
                }
                break;
        }
        return Input.GetKeyDown(key);
    }

    public static bool PlayerKeyPressed(PlayerIndex playerIndex, FunctionKeyType buttonType)
    {
        KeyCode key = KeyCode.Alpha0; //Default value, shouldn't cause issues
        switch (playerIndex)
        {
            case PlayerIndex.One:
                key = KeyCode.Space;
                break;
            case PlayerIndex.Two:
                key = KeyCode.Return;
                break;
        }
        return Input.GetKeyDown(key);
    }
}
