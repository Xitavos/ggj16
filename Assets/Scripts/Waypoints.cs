﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public class Waypoints : MonoBehaviour
{
	public Transform[] WaypointsArray;

	void Start()
	{
		Transform[] children = GetComponentsInChildren<Transform>();
		string[] childrenNames = new string[children.Length];

		for (int i = 0; i < children.Length; i++)
			childrenNames[i] = children[i].name;

		Array.Sort(childrenNames, children);
		WaypointsArray = children;
	}
}
