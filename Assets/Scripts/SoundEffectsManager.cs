﻿using UnityEngine;
using System.Collections;

public class SoundEffectsManager : MonoBehaviour
{
	[SerializeField] GameObject AudioPoint;

	public void PlaySound(Vector3 position, params AudioClip[] clips)
	{
		int clipIndex = Random.Range(0, clips.Length - 1);

		GameObject audiosource = Instantiate(AudioPoint, transform.position, Quaternion.identity) as GameObject;
		AudioPoint audiopoint = audiosource.GetComponent<AudioPoint>();
		audiopoint.Clip = clips[clipIndex];
	}
}
