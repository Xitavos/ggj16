﻿using UnityEngine;

public class BombMinion : GenericMinion
{
    [SerializeField] float towerDetectionRadius;
    [SerializeField] float explosionRadius;
    [SerializeField] int maxExplosionDamage;

    [SerializeField] GameObject explosionParticles;
    [SerializeField] LayerMask layerMask;
	[SerializeField] AudioClip[] explosionSounds;

    Collider[] hitColliders;
    Tower detectedTower;
	SoundEffectsManager soundEffectManager;
    MinionAttack minionAttack;

    bool isDead;
    bool hasDetectedTower;

    void Start ()
    {
        healthScript = GetComponentInChildren<Health>();
        minionMovement = GetComponent<MinionMovement>();
        minionAttack = GetComponent<MinionAttack>();
		soundEffectManager = FindObjectOfType<SoundEffectsManager>();
    }
	
	protected override void Update()
    {
        if (!isDead)
        {
            if (GetComponentInChildren<Health>() == null)
            {
                Explode(Physics.OverlapSphere(transform.position, explosionRadius));
            }

            //Collider[] detectedColliders = Physics.OverlapSphere(transform.position, towerDetectionRadius);
            //hasDetectedTower = false;

            Tower nearestTower = minionAttack.getClosestTower();

            if (nearestTower != null && (nearestTower.transform.position - transform.position).magnitude < towerDetectionRadius && nearestTower.belongsToPlayer != player)
            {
                hasDetectedTower = true;

                detectedTower = nearestTower;
            }

            /*foreach (Collider collider in detectedColliders)
            {
                if (collider.tag == "Tower")
                {
                    print("meow");

                    if (collider.gameObject.GetComponent<Tower>().belongsToPlayer != player)
                    {
                        hasDetectedTower = true;
                        detectedTower = collider.gameObject.GetComponent<Tower>();
                        break;
                    }
                }
            }*/

            if (hasDetectedTower)
            {
                detectedTower.Destroy();

                Explode(Physics.OverlapSphere(transform.position, explosionRadius));
            }
        }
	}

    void Explode(Collider[] hitColliders)
    {
        Instantiate(explosionParticles, transform.position, Quaternion.identity);

        for (int i = 0; i < hitColliders.Length; i++)
        {
            GenericMinion genMin = hitColliders[i].GetComponent<GenericMinion>();
            if (hitColliders[i].tag == "Tower")
            {
                (hitColliders[i].gameObject.GetComponent<Tower>() as Tower).Destroy();
            }
            else if (genMin != null)
            {
                DealDamage(genMin);
            }
        }

		soundEffectManager.PlaySound(transform.position, explosionSounds);

        Die();
    }

    void DealDamage (GenericMinion minion)
    {
        int damage;
        float distanceToMinion;

        distanceToMinion = Vector3.Distance(minion.gameObject.transform.position, transform.position);
        damage = Mathf.RoundToInt(maxExplosionDamage * (1 - (distanceToMinion / explosionRadius)));
        damage = Mathf.Clamp(damage, 0, maxExplosionDamage);

        minion.healthScript.TakeDamage(damage);
    }

    protected override void Die()
    {
        Spawner.instance.RemoveFromList(this, player, lane);
        Destroy(gameObject);
    }
}