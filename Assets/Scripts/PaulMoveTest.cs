﻿using UnityEngine;
using System.Collections;

public class PaulMoveTest : MonoBehaviour
{
    Rigidbody rb;
    Vector3 velocity;
    float moveSpeed = 3;

	void Start ()
    {
        rb = GetComponent<Rigidbody>();
   	}
	
	void Update ()
    {
        Vector3 moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        velocity = moveInput.normalized * moveSpeed;
    }

    void FixedUpdate ()
    {
        rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
    }
}
