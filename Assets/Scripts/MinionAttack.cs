﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

public class MinionAttack : MonoBehaviour
{
	[SerializeField] float rangedRange;
	[SerializeField] float meleeRange;
	[SerializeField] float attackDelay;
	public int Damage;

	[HideInInspector] public Lane lane;
	[HideInInspector] public int player;

    protected GenericMinion target;
    Rigidbody myRigidbody;
    protected MinionMovement minionMovement;
	protected GenericMinion genericMinion;
	bool isAttacking;
    Vector3 targetPosition;
    GenericMinion desiredTarget;

    List<GenericMinion> myList;
	List<GenericMinion>[] enemyLanes;

    protected virtual void Start ()
    {
        myRigidbody = GetComponent<Rigidbody>();
		minionMovement = GetComponent<MinionMovement>();
		genericMinion = GetComponent<GenericMinion>();

        StartCoroutine(wait(0.1f));

        genericMinion.player = player;
        genericMinion.lane = lane;
	}

    protected IEnumerator wait (float t)
    {
        yield return new WaitForSeconds(t);
    }

	protected virtual void Update()
	{
        Tower nearestTower = getClosestTower();

		enemyLanes = player == 1 ? Spawner.instance.playerOneLanes : Spawner.instance.playerTwoLanes;
		myList = player == 1 ? Spawner.instance.playerTwoLanes[(int)lane] : Spawner.instance.playerOneLanes[(int)lane];

		if (enemyLanes[(int)lane].Count != 0)
		{
			desiredTarget = enemyLanes[(int)lane][0];

            if(desiredTarget == null && nearestTower != null && nearestTower.belongsToPlayer != player)
            {
                desiredTarget = nearestTower.GetComponent<GenericMinion>();
            }

            if (desiredTarget != null)
			{
                if (nearestTower != null)
                {
                    if (nearestTower.belongsToPlayer != player && (nearestTower.transform.position - transform.position).magnitude < (desiredTarget.transform.position - transform.position).magnitude)
                    {
                        desiredTarget = nearestTower.GetComponent<GenericMinion>();
                    }
                }

                if ((desiredTarget.transform.position - transform.position).magnitude < rangedRange)
				{
					if (!isAttacking)
					{
						isAttacking = true;
						target = desiredTarget;
						StartCoroutine("BeginAttack");
					}
				}

				if ((desiredTarget.transform.position - transform.position).magnitude < meleeRange)
				{
					minionMovement.canMove = false;
				}
			}
		}
	}

    public Tower getClosestTower ()
    {
        float distance;
        float minTowerDistance = Mathf.Infinity;
        Tower nearestTower = null;
        Tower[] allTowers = FindObjectsOfType<Tower>();

        foreach (Tower tower in allTowers)
        {
            distance = Vector3.Distance(tower.transform.position, transform.position);
            if (distance < minTowerDistance)
            {
                minTowerDistance = distance;
                nearestTower = tower;
            }
        }

        return nearestTower;
    }
	
    IEnumerator BeginAttack()
    {
        while (true)
		{
			if (target == null)
			{
				isAttacking = false;
				minionMovement.canMove = true;

				//Logans added code
				if (enemyLanes[(int)lane] == null)
				{
					break;
				}

				if (enemyLanes[(int)lane].Count == 0)
				{
					break;
				}

				Debug.Log(enemyLanes[(int)lane]);
				Debug.Log(enemyLanes[(int)lane][0]);
				//Logans end

				if (enemyLanes[(int)lane][0] != null)
				{
					desiredTarget = enemyLanes[(int)lane][0];
				}

				break;
			}

            if (target != null)
            {
                if (target.GetComponent<GenericMinion>() != null)
                {
					if (target.GetComponent<GenericMinion>().healthScript != null)
					{
						target.GetComponent<GenericMinion>().healthScript.TakeDamage(Damage);
					}                    
                }
                else if (target.GetComponent<BombMinion>() != null)
                {
					if (target.GetComponent<BombMinion>().healthScript != null)
					{
						target.GetComponent<BombMinion>().healthScript.TakeDamage(Damage);
					}
                }
                else
                    break;
            }

            if (target == null)
                break;

			yield return new WaitForSeconds(attackDelay);
		}
    }
}
