﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tower : MonoBehaviour
{
    [HideInInspector] public int belongsToPlayer;

    [SerializeField] Vector3 targetPosition;
	[SerializeField] float riseSpeed;
    [SerializeField] float attackDelay;
    [SerializeField] int towerDamage;
    [SerializeField] float lifetime;

    [SerializeField] GameObject destructionParticles;

    [SerializeField] ParticleSystem particles;
    [SerializeField] LineRenderer line;
	[SerializeField] AudioClip[] erectSoundEffects;
    [SerializeField] private AudioClip[] erectVoiceEffects;
    
    [SerializeField] Color[] colors =
    {
        new Color(229, 0, 0, 170),
        new Color(0, 175, 255, 170)
    };

    private string[] topPoints = { "13", "12", "11", "10", "09" };
    private string[] midPoints = { "00", "08", "07", "06", "14" };
    private string[] botPoints = { "05", "04", "03", "02", "01" };

    SoundEffectsManager soundEffectsManager;
    bool isRising = true;
    bool isAttacking;

    List<GenericMinion> currentEnemies = new List<GenericMinion>();

	void Start ()
    {
		soundEffectsManager = FindObjectOfType<SoundEffectsManager>();
		soundEffectsManager.PlaySound(transform.position, erectSoundEffects);
        soundEffectsManager.PlaySound(transform.position, erectVoiceEffects);
        particles.startColor = colors[belongsToPlayer];

        line.enabled = false;
        line.SetPosition(0, particles.gameObject.transform.position);
    }

    public void FaceToNext(Lane lane, int player)
    {
        string current = transform.parent.name.Split(' ')[1];
        string toPointTo = "";
        switch (lane)
        {
            case Lane.Top:
                toPointTo = topPoints[Array.IndexOf(topPoints, current) + (player == 0 ? 1 : -1)];
                break;
            case Lane.Middle:
                toPointTo = midPoints[Array.IndexOf(midPoints, current) + (player == 0 ? 1 : -1)];
                break;
            case Lane.Bottom:
                toPointTo = botPoints[Array.IndexOf(botPoints, current) + (player == 0 ? 1 : -1)];
                break;
        }
        GameObject nextWaypoint = GameObject.Find("Target " + toPointTo);
        transform.LookAt(nextWaypoint.transform.position);
    }
	
	void Update ()
    {
		if (isRising)
		{
			transform.localPosition = Vector3.Lerp(transform.localPosition, targetPosition, Time.deltaTime * riseSpeed);
		}

		if ((targetPosition - transform.localPosition).sqrMagnitude < 0.01f)
		{
			isRising = false;
		}

        if (!isAttacking && currentEnemies.Count != 0)
        {
            StartCoroutine(Attack(currentEnemies[0]));
        }
	}

    public void Destroy ()
    {
        Instantiate(destructionParticles, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    void OnTriggerEnter (Collider other)
    {
        print("hi");

        if (other.GetComponent<GenericMinion>() != null)
        {
            GenericMinion otherMinion = other.GetComponent<GenericMinion>();

            if (otherMinion != null && otherMinion.player != belongsToPlayer)
            {
                currentEnemies.Add(otherMinion);
            }
        }
    }

    IEnumerator Attack (GenericMinion enemy)
    {
        isAttacking = true;

        while (enemy != null)
        {
            line.SetPosition(1, enemy.transform.position + Vector3.up);
            line.enabled = true;
            yield return new WaitForSeconds(lifetime);

            line.enabled = false;
            line.SetPosition(1, particles.gameObject.transform.position);

            enemy.healthScript.TakeDamage(towerDamage);

            yield return new WaitForSeconds(attackDelay - lifetime);
        }

        isAttacking = false;
        currentEnemies.Remove(enemy);
    }
}
