﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

class PentagramUI : MonoBehaviour
{
    [SerializeField] private GameObject playerInputObject;
    [SerializeField] private GameObject[] minionIcons;
    [SerializeField] private Vector3 topScale;

    private PlayerInput playerInput;
    private Vector3[] minionIconPositions;
    private Vector3[] minionIconScales;
    private int currentIndex = 0;
    private int maxIndex = 4;

    void Start()
    {
        playerInput = playerInputObject.GetComponent<PlayerInput>();
        playerInput.SelectionChanged += OnSelectionChanged;
        playerInput.Failed += OnComboFailed;
        playerInput.Completed += OnComboCompleted;
        MoveSelector(0);
        minionIconPositions = minionIcons.Select(g => g.transform.localPosition).ToArray();
        minionIconScales = new Vector3[maxIndex+1];
        for (int i = 0; i <= maxIndex; i++)
            minionIconScales[i] = Vector3.one;
        minionIconScales[1] = Vector3.one * 2f;
    }

    private void OnComboFailed(object sender, FailedArgs e)
    {
        StartCoroutine(Cooldown(minionIcons[1], e.StartTime, e.EndTime));
    }

    private void OnComboCompleted(object sender, CompletedArgs e)
    {
        StartCoroutine(Cooldown(minionIcons[1], e.StartTime, e.EndTime));
    }

    private IEnumerator Cooldown(GameObject minionIcon, float startTime, float endTime)
    {
        Image image = minionIcon.GetComponent<Image>();
        while (Time.time < endTime)
        {
            image.fillAmount = (Time.time - startTime) / (endTime - startTime);
            yield return null;
        }
        image.fillAmount = 1f;
    }

    void Update()
    {
        for (int i = 0; i <= maxIndex; i++)
        {
            minionIcons[i].transform.localPosition = Vector3.Lerp(minionIcons[i].transform.localPosition, minionIconPositions[i], Time.deltaTime * 10f);
            minionIcons[i].transform.localScale = Vector3.Lerp(minionIcons[i].transform.localScale, minionIconScales[i], Time.deltaTime * 9f);
        }
    }

    private void OnSelectionChanged(object sender, SelectionChangedArgs e)
    {
        MoveSelector(e.Direction);
    }

    void MoveSelector(int direction)
    {
        currentIndex += direction;
        if (currentIndex < 0)
            currentIndex = maxIndex;
        else if (currentIndex > maxIndex)
            currentIndex = 0;
        if (direction == 1)
            RotateRight();
        else
            RotateLeft();
    }

    private void RotateLeft()
    {
        GameObject last = minionIcons[maxIndex];
        for (int i = maxIndex; i > 0; i--)
            minionIcons[i] = minionIcons[i - 1];
        minionIcons[0] = last;
    }

    private void RotateRight()
    {
        GameObject first = minionIcons[0];
        for (int i = 0; i < maxIndex; i++)
            minionIcons[i] = minionIcons[i + 1];
        minionIcons[maxIndex] = first;
    }
}
