﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using XInputDotNetPure;

public class ButtonUI : MonoBehaviour
{
    [SerializeField] private GameObject left;
    [SerializeField] private GameObject right;
    [SerializeField] private Sprite[] buttonSprites;
    [SerializeField] private Sprite[] keyOneSprites;
    [SerializeField] private Sprite[] keyTwoSprites;

    public void Initialize(ComboPart c, PlayerIndex playerIndex)
    {
        Image buttonImage = gameObject.GetComponent<Image>();
        if (PlayerPrefs.GetString("UI") == "Controller")
        {
            buttonImage.sprite = buttonSprites[(int)c.Button];
        }
        else
        {
            if (playerIndex == PlayerIndex.One)
                buttonImage.sprite = keyOneSprites[(int)c.Button];
            else
                buttonImage.sprite = keyTwoSprites[(int)c.Button];
        }
        if (!c.LeftTrigger)
            Destroy(left);
        if (!c.RightTrigger)
            Destroy(right);
    }
}
