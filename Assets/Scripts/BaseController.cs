﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BaseController : MonoBehaviour
{
    public float HitDelay = 1.0f;
    [Range(0, 1)] public int PlayerIndex;

	public int InitialHealth = 50;
    [SerializeField] bool devMode;
    [SerializeField] GameObject fireworks, explosion;
    [SerializeField] GameObject playerOneInput, playerTwoInput, GameOverText, Fade;
    [SerializeField] BaseController otherPlayerBase;
    [SerializeField] Text playerWinText;
    [SerializeField] Health healthBar;

    public int CurrentHealth { get; private set; }
    public bool hasWon;

    void Start()
    {
        CurrentHealth = InitialHealth;
    }

    void Update()
    {
        if (devMode)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                takeHit(10);
            }
        }
    }
    
    void takeHit(int damage)
    {
        if (!hasWon)
        {
            CurrentHealth -= damage;
            healthBar.TakeDamageBase(damage);

            if (CurrentHealth <= 0)
            {
                lose();
                otherPlayerBase.win();
            }
        }
    }

    IEnumerator WaitForGameEnd()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(0);
    }

    public void win ()
    {
        if (!hasWon)
        {
            hasWon = true;
            otherPlayerBase.hasWon = true;

            Instantiate(fireworks, transform.position, fireworks.transform.rotation);

            Destroy(playerOneInput);
            Destroy(playerTwoInput);

            otherPlayerBase.lose();

            GameOverText.SetActive(true);
            Fade.SetActive(true);
            playerWinText.text = "Player " + (PlayerIndex + 1) + " Wins!";

            StartCoroutine(WaitForGameEnd());
        }
    }

    public void lose ()
    {
        if (!hasWon)
        {
            Instantiate(explosion, transform.position, Quaternion.identity);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Minion" && other.gameObject.GetComponent<GenericMinion>().player != PlayerIndex)
        {
            MinionMovement minionMovement = other.gameObject.GetComponent<MinionMovement>();
            GenericMinion genericMinion = other.gameObject.GetComponent<GenericMinion>();

            Spawner.instance.RemoveFromList(genericMinion, genericMinion.player, genericMinion.lane);

            genericMinion.GetComponent<Collider>().enabled = false;

            StartCoroutine(WaitToStopMovement(HitDelay, genericMinion, minionMovement));
        }
    }

    IEnumerator WaitToStopMovement(float t, GenericMinion genMin, MinionMovement minMov)
    {
        yield return new WaitForSeconds(t);

        minMov.canMove = false;

        takeHit(1);

        Destroy(genMin.gameObject, 1);
    }
}