﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PlayMusicOnStart : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
	    GetComponent<AudioSource>().Play();
	}
}
